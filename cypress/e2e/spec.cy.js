describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://morningnews-preprod.theguichard.fr')
  })

  it('Connect succes', () => {
   cy.visit('http://morningnews-preprod.theguichard.fr') 
   cy.get('.fa-user').click()
   cy.get('#signInUsername').type('test')
   cy.get('#signInPassword').type('test')
   cy.get('.Header_logoutSection__KHqw2 > p').should('not.exist')
   cy.get('#connection').click()
   cy.get('.Header_logoutSection__KHqw2 > p').should('exist')
  })

  it('Devrait permettre la mise en favori des articles', () => {
    cy.visit('http://morningnews-preprod.theguichard.fr') 
    cy.get('.fa-user').click()
    cy.get('#signInUsername').type('test')
    cy.get('#signInPassword').type('test')
    cy.get('#connection').click()
    cy.wait(500)
    cy.get('.Home_articlesContainer__L2onQ > :nth-child(1) > .Article_articleHeader__z8QZl > .fa-bookmark > path').click()
    cy.wait(500)
    cy.get('.Header_linkContainer__hNrE8 > :nth-child(2)').click()
    cy.wait(500)
    cy.url().should('include', 'bookmarks')
    cy.wait(500)
    cy.get('.Bookmarks_container__yWQBe').should('exist')
   })

   it('Devrait permettre de masquer un articles et de le démasquer', () => {
    cy.visit('http://morningnews-preprod.theguichard.fr') 
    cy.get('.fa-user').click()
    cy.get('#signInUsername').type('test')
    cy.get('#signInPassword').type('test')
    cy.get('#connection').click()
    let myText
    cy.get('.Home_articlesContainer__L2onQ > :nth-child(1) > .Article_articleHeader__z8QZl > h3').invoke('text').then(text => {
      myText = text.trim()
      expect(myText).to.not.be.empty
    cy.get('.Home_articlesContainer__L2onQ > :nth-child(1) > .Article_articleHeader__z8QZl > .fa-eye-slash > path').click()
    cy.get('.Home_articlesContainer__L2onQ > :nth-child(1) > .Article_articleHeader__z8QZl > h3').should('not.contain', myText)
    cy.get('.fa-eye').click()
    cy.get('.Home_articlesContainer__L2onQ > :nth-child(1) > .Article_articleHeader__z8QZl > h3').should('contain', myText)
    })
   })
})
