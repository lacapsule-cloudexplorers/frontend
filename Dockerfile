FROM node:16-alpine

COPY . .
RUN yarn install
RUN yarn build
EXPOSE 3001
CMD ["yarn","dev"]
